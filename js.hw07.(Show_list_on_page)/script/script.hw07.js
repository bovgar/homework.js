const basicArray = ["Iggy Pop", "Klaus Schulze", "Pink Floyd", "Kraftwerk", "Deep Purple", "Nirvana", "Einstürzende Neubauten", "Dead Can Dance", "Tom Waits", "Van Der Graaf Generator"];
const contentOnPage = getMarkingString(basicArray);

showOnPage(contentOnPage);

function getMarkingString(array) {
    const newArray = array.map((item) => `<li>${item}</li>`);
    return newArray.join("");
};

function showOnPage(content) {
    document.getElementById("list").innerHTML = content;
}