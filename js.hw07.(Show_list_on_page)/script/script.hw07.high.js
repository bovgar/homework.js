// basic level
const basicArray = ["Iggy Pop", "Klaus Schulze", "Pink Floyd", "Kraftwerk", "Deep Purple", "Nirvana", "Einstürzende Neubauten", "Dead Can Dance", "Tom Waits", "Van Der Graaf Generator"];
const contentOnPage = getMarkingString(basicArray);
showOnPage(contentOnPage, "list");

function getMarkingString(array) {
    const newArray = array.map((item) => `<li>${item}</li>`);
    return newArray.join("");
};

//advance level
const someObj = {something: "nothing", somewhere: "nowhere"}
const iggyPop = ["punk rock", "hard rock", someObj];
const klausSchulze = ["ambient", "krautrock", "electronic", iggyPop];
const pinkFloyd = ["psychedelic", "progressive rock", klausSchulze];
const eNB = {style: "industrial", style2: pinkFloyd, country: "Deutschland", frontman: "Blixa Bargeld",};
const basicArray2 = ["Iggy Pop", iggyPop, "Klaus Schulze",klausSchulze, "Pink Floyd", pinkFloyd, "Kraftwerk", "Deep Purple", "Nirvana", "Einstürzende Neubauten", eNB, "Dead Can Dance", "Tom Waits", "Van Der Graaf Generator"];

const from = 20;
const to = 0;
timer(from, to);

const contentOnPage2 = createMarkingString(basicArray2);
showOnPage(contentOnPage2, "listAdvance");

function createMarkingString(array) {
    let string = "";
    for (let key of array) {
        if (Array.isArray(key)) {
            string += `<li><ul>${createMarkingString(key)}</ul></li>`;
        }
        else if(typeof (key) === "object") {
                string +=`<li><ul>${createStringFromObject(key)}</ul></li>`;
        }
        else {
            string += `<li>${key}</li>`;
        }
    }
    return string;
}

function createStringFromObject (obj) {
    let string = "";
    for (let key in obj) {
        if (Array.isArray(obj[key])){
            string += `${createMarkingString(obj[key])}`;
        }
        else if (typeof (obj[key]) === "object"){
            string += `<ul>${key}${createStringFromObject(obj[key])}</ul>`
        }
        else {
            string += `<li>${key}: ${obj[key]}</li>`;
        }
    }
    return string;
}

function showOnPage(content, ulId) {
    document.getElementById(ulId).innerHTML = content;
};

function timer(start, end) {
    const timerId = setInterval(function () {
        // console.log(start);
        showOnPage(`hasta la vista, Baby: ${start}`, "timer");
        if (!start){
            clearInterval(timerId)
            showOnPage("", "timer");
            showOnPage("", "listAdvance");
        }
        start--;
    }, 1000, start, end);
}

/*
function getMarkingString2(array) {
let string = "";
for (let key of array) {
    if (Array.isArray(key)) {
        string = string.substr(0, string.length-5) +`<ul>${getMarkingString(key)}</ul></li>`;
        console.log(string);
    }
        else if(typeof (key) === "object") {
        let strObj ="";
        for (let keyLev2 in key) {
            strObj += `<li>${keyLev2}: ${key[keyLev2]}</li>`;
        }
        string = string.substr(0, string.length-5) + `<ul>${strObj}</ul></li>`;
    }
    else {
        string += `<li>${key}</li>`;
    }
}
    return string;
}
 */
