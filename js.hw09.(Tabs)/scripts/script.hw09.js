// var#1 - using event
const tabs = document.getElementById("tabs");
tabs.addEventListener("click", function (event) {
    //set active Tab
    const liNode = tabs.children;
    for (let i = 0; i < liNode.length; i++) {
        liNode[i].classList.remove("active");
    }
    event.target.classList.add("active");

    //find Content corresponding to active Tab
    const tabContent = document.querySelectorAll(".tabs-content-list");
    tabContent.forEach(function(item) {
        item.classList.remove("active");
    });
    const tabId = event.target.dataset.tab;
    const liContent = document.getElementById(tabId);
    liContent.classList.add("active");
});

/*
// var#2
const tabElements = document.querySelectorAll(".tabs-title");
tabElements.forEach(function (item) {
    item.addEventListener("click", function(){
        const parentUl = this.closest(".tabs");
        const liNode = parentUl.children;
        for (let i =0; i < liNode.length; i++){
            liNode[i].classList.remove("active");
        };
        item.classList.add("active");

        const tabContent = document.querySelectorAll(".tabs-content-list");
        tabContent.forEach(function(item) {
            item.classList.remove("active");
        });
        const tabId = this.dataset.tab;
        const liContent = document.getElementById(tabId);
        liContent.classList.add("active");
    });
});
 */
