/*
Матод массива forEach() перебирает все элементы массива. Для каждого элемента вызывает функцию колбек.
Функция callback(elem, i, array) принимает аргументамы:
elem - элемент массива, i - индекс элемента, array - массив, к которому применяется метод
 */

const randomArray = ["12", 13, "random text1", 0, "0", {name: "Klaus", surname: "Schulze"}, -5, null, "null", "random text2", true, "true", 15, false, "false", undefined, "undefined", [-1,0,5]];
const dataType = prompt("Choose data type (number, string, boolean, null, undefined, object)", "string");
const filterArray = filterBy(randomArray, dataType);
console.log(`basic array:\n${randomArray}`);
console.log(`filtered array, without elements "${dataType}" data type:\n${filterArray}`);

function filterBy (array, type) {
    const newArray = array.filter(function(elem) {
        return typeof(elem) !== type;
    });
    return newArray;
}
